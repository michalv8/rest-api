<?php

declare(strict_types=1);

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class PostModel
{
    /**
     * @var string
     *
     * @Assert\Length(min = 3, max = 128)
     */
    public $title;

    /**
     * @var string
     *
     * @Assert\Length(min = 20, max = 256)
     */
    public $content;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email
     */
    public $author;
}