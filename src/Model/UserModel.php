<?php

declare(strict_types=1);

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserModel
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank(groups = {"create"})
     */
    public $username;

    /**
     * @var string|null
     *
     * @Assert\NotBlank
     */
    public $firstName;

    /**
     * @var string|null
     *
     * @Assert\NotBlank
     */
    public $lastName;

    /**
     * @var string|null
     *
     * @Assert\NotBlank
     * @Assert\Length(min = 8)
     */
    public $password;
}
