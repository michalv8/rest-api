<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Traits\FormErrorsTrait;
use App\Factory\UserFactory;
use App\Factory\UserModelFactory;
use App\Factory\UserViewFactory;
use App\Form\UserType;
use App\Model\UserModel;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    use FormErrorsTrait;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var UserFactory
     */
    private $factory;

    /**
     * @var UserViewFactory
     */
    private $viewFactory;

    /**
     * @var UserModelFactory
     */
    private $modelFactory;

    public function __construct(UserRepository $repository, SerializerInterface $serializer, UserFactory $factory, UserViewFactory $viewFactory, UserModelFactory $modelFactory)
    {
        $this->repository = $repository;
        $this->serializer = $serializer;
        $this->factory = $factory;
        $this->viewFactory = $viewFactory;
        $this->modelFactory = $modelFactory;
    }

    /**
     * @Route("/users", name = "get_users", methods = {"GET"}, requirements = { "_format" = "json" })
     */
    public function getUsersAction(): JsonResponse
    {
        $users = $this->repository->findAll();

        return $this->json(array_map([$this->viewFactory, "create"], $users));
    }

    /**
     * @Route("/users/{userId}", name = "get_user", methods = {"GET"}, requirements = { "_format" = "json" })
     */
    public function getUserAction(string $userId): JsonResponse
    {
        $user = $this->repository->findOneById($userId);

        if (!$user) {
            return $this->json([
                'errors' => ['User not found']
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json($this->viewFactory->create($user));
    }

    /**
     * @Route("/users", name = "post_user", methods = {"POST"}, requirements = { "_format" = "json" })
     */
    public function postUserAction(Request $request): JsonResponse
    {
        $data = \json_decode($request->getContent(), true);

        $form = $this->createForm(UserType::class, null, [
            'validation_groups' => ['Default', 'create'],
        ]);

        $form->submit($data);

        if (!$form->isValid()) {
            return $this->json($this->extractErrors($form), Response::HTTP_BAD_REQUEST);
        }

        $user = $this->factory->createFromModel($form->getData());

        $this->repository->save($user);

        return $this->json(null, Response::HTTP_CREATED, [
            'Location' => $this->generateUrl('get_user', [
                'userId' => $user->getId(),
            ])
        ]);
    }

    /**
     * @Route("/users/{userId}", name = "put_user", methods = {"PUT"}, requirements = { "_format" = "json" })
     */
    public function putUserAction(Request $request, string $userId): JsonResponse
    {
        $user = $this->repository->findOneById($userId);

        if (!$user) {
            return $this->json([
                'errors' => ['User not found']
            ], Response::HTTP_NOT_FOUND);
        }

        $userModel = $this->modelFactory->create($user);

        $form = $this->createForm(UserType::class, $userModel);

        $data = \json_decode($request->getContent(), true);

        $form->submit($data);

        if (!$form->isValid()) {
            return $this->json($this->extractErrors($form), Response::HTTP_BAD_REQUEST);
        }

        $user->hydrate($userModel);

        $this->repository->save($user);

        return $this->json($this->viewFactory->create($user));
    }
}
