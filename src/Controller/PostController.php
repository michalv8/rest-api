<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @var PostRepository
     */
    private $repository;

    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/posts", name = "get_posts", methods = {"GET"}, requirements={"_format" = "json"})
     */
    public function getPostsAction(): JsonResponse
    {
        $posts = $this->repository->findAll();

        return $this->json($posts);
    }
}