<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name = "posts")
 */
class Post
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type = "guid")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type = "string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type = "string")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type = "string")
     */
    private $author;

    public function __construct(string $id, string $title, string $content, string $author)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->author = $author;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }
}