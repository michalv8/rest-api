<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\User;
use App\View\UserView;

class UserViewFactory
{
    public function create(User $user): UserView
    {
        return new UserView($user);
    }
}