<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\User;
use App\Model\UserModel;

class UserModelFactory
{
    public function create(User $user): UserModel
    {
        $model = new UserModel();

        $model->username = $user->getUsername();
        $model->password = $user->getPassword();
        $model->firstName = $user->getFirstName();
        $model->lastName = $user->getLastName();

        return $model;
    }
}